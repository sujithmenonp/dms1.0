var UserAccountModel = require("../model/UserAccountModel")();
var APIConstants = require("../constants/APIConstants");
var LoginValidator = require("../validator/UserLoginRequestValidator");
var EncryptionUtil = require('../util/EncryptionUtil');
var Logger = require('../logger/Logger')();
 
var validateParamsForPOSTPassword = function(req, res){
	
	var success = true;
	var response = LoginValidator.validateParamsForPOSTPassword(req);

	if(response != ""){
		res.json(response);
		success = false;
	}
	return success;
}

var setUserAccountPassword = function(req,res){
	
	UserAccountModel.find({
		where: {
			mobile_details_id: req.body.mobile_details_id
		} 
	}).then(function(existingUserAccount) {
		 
		if (existingUserAccount) { // if the record exists in the db
			
			//TODO: Encrypt the password before storing it.
			var encryptedPassword=	EncryptionUtil.encrypt(req.body.password);
  
      
                console.log(encryptedPassword);

				existingUserAccount.updateAttributes({
					  password: encryptedPassword
			    }).then(function() {
		    		res.json(APIConstants.PASSWORD_SET_SUCCESSFULLY);
			    });
		
		

		  }
		else{
			  res.json(APIConstants.ERROR_SETTING_PASSWORD);
		  }
		});
}

var setPassword = function (req, res) {

    var success = validateParamsForPOSTPassword(req, res);
         
    if(!success) return ;
            
    setUserAccountPassword(req, res);
}

var verifyUserPassword = function (username, password, done){

	Logger.info("Start: verifyUserPassword: ");
	var passwordInDB =  "Welcome100";//EncryptionUtil.decrypt('Welcome100') ;

	if(passwordInDB === password){
		Logger.info("User With Mobile Number:"+ username+" Successfully Authenticated");
		done(null, { id: username } );
	}else{
		Logger.info("User With Mobile Number:"+ username+" Failed To Authenticate");
		done(null, null);
	}
	Logger.info("Completed: verifyUserPassword: ");
};


module.exports = {
	setPassword: setPassword,
	verifyUserPassword: verifyUserPassword
}
            
