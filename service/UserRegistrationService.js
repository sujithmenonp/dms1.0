var UserModel = require("../model/UserModel")();
var MobileDetailsModel= require("../model/MobileDetailsModel")();
var AddressInfoModel = require("../model/AddressInfoModel")();
var UserAccountModel = require("../model/UserAccountModel")();
var APIConstants = require("../constants/APIConstants");
var RegistrationParamValidator = require("../validator/UserRegistrationRequestValidator");
var Logger = require('../logger/Logger')();

var validateRequest = function(req, res){
	
	var success = true;
	var response = RegistrationParamValidator.validateParams(req);

	if(response != ""){
		res.json(response);
		success = false;
	}
	
	return success;
}

var createAddressAndAccount = function(req, res, user, mobile){

	var addressInfoModel = createAddressInfoModel(req);
	
	addressInfoModel.user_profile_id = user.user_profile_id;
	
	addressInfoModel.create(addressInfoModel).then(function (addedAddress){
		
	if(addedAddress){
			
			var userAccountModel = UserAccountModel;
			
			userAccountModel.user_profile_id = user.user_profile_id;
			userAccountModel.mobile_details_id = mobile.mobile_details_id;
			userAccountModel.create(userAccountModel).then( function (addedUserAccount){
			
				if(addedUserAccount){
					//TODO: Generate OTP and Store It in DB.
					//TODO: Create an OTPService and call it here.
					res.json(APIConstants.REGISTERED_SUCCESS);
				}else{
					console.log("ERROR: User Account could not be created for user : "+user.user_profile_id);
					res.json(APIConstants.ERROR_REGISTRATION);
				}
			});
		}else{
			console.log("ERROR: Address could not be created for user : "+user.user_profile_id);
			res.json(APIConstants.ERROR_REGISTRATION);
		}
	});
}

var findOrCreateMobileDetails = function(req,res, user){
	
	var mobileDetailsModel = createMobileDetailsModel(req);
	
	MobileDetailsModel.findOrCreate({
		where: {
			mobile_number: mobileDetailsModel.mobile_number.trim()
		},
        defaults: {
        	user_profile_id: user.user_profile_id
        }
	}).spread(function (mobiledetails, createdMobile) {
		if(!createdMobile){
			res.json(APIConstants.MOBILE_EXISTS);
			user.destroy();
		}else{
			createAddressAndAccount(req, res, user, mobiledetails);
		}
	});
}
var findOrCreateUser = function (req, res){
	
	var userModel = createUserModel(req);
	
	userModel.findOrCreate({
	    	where: {
	    		email: userModel.email.trim()
	    	},
	    	defaults: {
	    		user_profile_id: userModel.user_profile_id,
	        	first_name: userModel.first_name,
	        	second_name: userModel.second_name,
	        	last_name: userModel.last_name,
	        	email: userModel.email,
	        	gender: userModel.gender,
	        	dateOfBirth: userModel.dateOfBirth,
	           	type: userModel.type
	    	}
	    	
	    }).spread(function(user, created){
	    	if(!created){
	        	res.json(APIConstants.USER_EXISTS);
	    	}else{
	          findOrCreateMobileDetails(req,res, user);
	    	}
	    });
}

var register = function (req, res) {

	Logger.info("Request For Registration");

	var success = validateRequest(req, res);
         
    if(!success) return ;

    findOrCreateUser(req, res);
}

var createUserModel = function(req){
	var userModel = UserModel;
	userModel.first_name= req.body.first_name;
	userModel.second_name= req.body.second_name;
	userModel.last_name= req.body.last_name;
	userModel.email= req.body.email;
	userModel.gender= req.body.gender;
	userModel.dateOfBirth= req.body.dateOfBirth;
	userModel.type= req.body.type;
	userModel.newColumn= req.body.newColumn;
	return userModel;
}
   
var createAddressInfoModel = function(req){
	var addressInfoModel = AddressInfoModel;
	addressInfoModel.address_1= req.body.address_1;
	addressInfoModel.address_2= req.body.address_2;
	addressInfoModel.address_3= req.body.address_3;
	addressInfoModel.city= req.body.city;
	addressInfoModel.pincode= req.body.pincode;
	addressInfoModel.state= req.body.state;
	addressInfoModel.latitude= req.body.latitude;
	addressInfoModel.longitude= req.body.longitude;
	addressInfoModel.landmark= req.body.landmark;
	
	return addressInfoModel;
}

var createMobileDetailsModel = function(req){
	var mobileDetailsModel = MobileDetailsModel;
	mobileDetailsModel.mobile_number= req.body.mobile_number;
	mobileDetailsModel.user_profile_id = req.body.email;
	
	return mobileDetailsModel;
}

module.exports = {
	register: register
}
            
