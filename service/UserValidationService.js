var MobileDetailsModel= require("../model/MobileDetailsModel")();
var UserAccountModel = require("../model/UserAccountModel")();
var APIConstants = require("../constants/APIConstants");
var UserValidationRequestValidator = require("../validator/UserValidationRequestValidator");


var createUserAccountModel = function(req){
	
	var userAccountModel =  UserAccountModel;
	userAccountModel.mobile_details_id = req.body.mobile_details_id;
	userAccountModel.user_profile_id = req.body.user_profile_id;
	userAccountModel.otp = req.body.otp;
	return userAccountModel;
}

var updateExistingUserAccount = function(existingUserAccount,req, res){
	
	//TODO: Add logic to check for the expiry of OTP
	if(existingUserAccount.otp == req.body.otp){
	
		//validation success. update mobile active state
		MobileDetailsModel.find({
			where: {
				mobile_details_id: existingUserAccount.mobile_details_id
			} 
		}).then(function(existingMobileDetails) {
			 
	
			if (existingMobileDetails) { // if the record exists in the db
				
				if(existingMobileDetails.active == "T"){
					res.json(APIConstants.MOBILE_ALREADY_ACTIVATED);
				}
				else{
					
					 existingMobileDetails.updateAttributes({
						  active: "T"
				    }).then(function() {
			    		res.json(APIConstants.OTP_VALIDATION_SUCCESS);

				    });
				}
			  }
			else{
				  res.json(APIConstants.OTP_VALIDATION_FAILED);
			  }
			 
			});
		
	}else{
		res.json(APIConstants.OTP_VALIDATION_FAILED);
	}
}
var findAndUpdateValidationStatus = function(req, res){

	var userAccountModel = createUserAccountModel(req);
	
	UserAccountModel.find({
		where: {
			mobile_details_id: userAccountModel.mobile_details_id,
			user_profile_id: userAccountModel.user_profile_id
		}
	}).then(function (existingAccount){
		
		if(existingAccount){
			return updateExistingUserAccount(existingAccount, req, res);
		}
		else{
			res.json(APIConstants.OTP_VALIDATION_FAILED);
		}
	});
}

var validateRequest= function(req, res){
	
	var success = true;
	var status = UserValidationRequestValidator.checkMandatoryFields(req);
	
	if(status != ""){
		res.json(status);
		success = false;
	}
	return success;
}

var validate= function(req, res){
	
	var success = validateRequest(req,res);
	
	if(!success) return ;
	
	findAndUpdateValidationStatus(req, res);
}

module.exports = {
		validate: validate
}