CREATE DATABASE `dms` /*!40100 DEFAULT CHARACTER SET latin1 */;

CREATE TABLE `user_address` (
  `user_address_id` bigint(20) NOT NULL,
  `user_login_id` bigint(20) NOT NULL,
  `address_line_1` varchar(100) NOT NULL,
  `address_line_2` varchar(100) DEFAULT NULL,
  `address_line_3` varchar(100) DEFAULT NULL,
  `landmark` varchar(200) DEFAULT NULL,
  `town` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `pin` char(6) DEFAULT NULL,
  `address_type` tinyint(4) DEFAULT NULL,
  `insert_ts` bigint(20) NOT NULL,
  `update_ts` bigint(20) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `user_session_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_address_id`),
  KEY `ua_ul_user_login_id_fk_idx` (`user_login_id`),
  CONSTRAINT `ua_ul_user_login_id_fk` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`user_login_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `user_gps` (
  `user_gps_id` bigint(20) NOT NULL,
  `user_address_id` bigint(20) NOT NULL,
  `latitute` double NOT NULL,
  `longitude` double NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `last_used_ts` bigint(20) DEFAULT NULL,
  `insert_ts` bigint(20) NOT NULL,
  `update_ts` bigint(20) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `user_session_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_gps_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `user_login` (
  `user_login_id` bigint(20) NOT NULL,
  `user_profile_id` bigint(20) NOT NULL,
  `login` char(15) NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  `otp` char(6) DEFAULT NULL,
  `otp_gen_ts` bigint(20) DEFAULT NULL,
  `otp_exp_ts` bigint(20) DEFAULT NULL,
  `otp_used` tinyint(4) DEFAULT NULL,
  `insert_ts` bigint(20) NOT NULL,
  `update_ts` bigint(20) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `user_session_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_login_id`),
  UNIQUE KEY `user_login_UNIQUE` (`login`),
  KEY `ul_up_user_profile_id_fk_idx` (`user_profile_id`),
  CONSTRAINT `ul_up_user_profile_id_fk` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profile` (`user_profile_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `user_profile` (
  `user_profile_id` bigint(20) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `age` smallint(6) DEFAULT NULL,
  `dob` char(10) DEFAULT NULL,
  `insert_ts` bigint(20) NOT NULL,
  `update_ts` bigint(20) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `user_session_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `user_session` (
  `user_session_id` bigint(20) NOT NULL,
  `user_login_id` bigint(20) NOT NULL,
  `login_ts` bigint(20) NOT NULL,
  `logout_ts` bigint(20) DEFAULT NULL,
  `login_mode` tinyint(4) NOT NULL,
  `login_data` varchar(200) DEFAULT NULL,
  `insert_ts` bigint(20) NOT NULL,
  `update_ts` bigint(20) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`user_session_id`),
  KEY `us_ul_user_login_id_fk_idx` (`user_login_id`),
  CONSTRAINT `us_ul_user_login_id_fk` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`user_login_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `vendor_login` (
  `vendor_login_id` bigint(20) NOT NULL,
  `login` varchar(45) NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  `insert_ts` bigint(20) NOT NULL,
  `update_ts` bigint(20) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `user_session_id` bigint(20) NOT NULL,
  `vendor_logincol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`vendor_login_id`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

