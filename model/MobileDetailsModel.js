var Sequelize = require('sequelize');
var sequelize = require('../server/Sequelize')();

var instance;

var getInstance = function () {

    if(instance) return instance;

    instance = sequelize.define("mobile_detail", {
        mobile_details_id: { primaryKey: true, type: Sequelize.INTEGER, autoIncrement: true },
        mobile_number: Sequelize.STRING,
		active: {type: Sequelize.STRING, defaultValue: "F"},
		user_profile_id: Sequelize.STRING,
		deleted: {type: Sequelize.STRING, defaultValue: "F" }
    });

    return instance;

};

module.exports = getInstance;