var Sequelize = require('sequelize');
var sequelize = require('../server/Sequelize')();

var instance;
var getInstance = function () {

	if(instance) return instance;

    instance = sequelize.define("user_account", {
    	user_account_id: { primaryKey: true, type: Sequelize.INTEGER, autoIncrement: true},
        mobile_details_id: Sequelize.INTEGER,
        user_profile_id: Sequelize.INTEGER,
		otp: Sequelize.STRING,
    	otp_gen_ts:  Sequelize.STRING,
		otp_exp_ts: Sequelize.STRING,
		otp_used: Sequelize.STRING,
		source_reg: Sequelize.STRING,
		password: Sequelize.STRING,
		login_id: Sequelize.STRING,
		deleted: { type: Sequelize.STRING, defaultValue: "F"}
    });

	return instance;
};
module.exports = getInstance;