var Sequelize = require('sequelize');
var sequelize = require('../server/Sequelize')()	;

var instance;

var getInstance = function () {

	if(instance) return instance;

    instance = sequelize.define("address_info", {
    	address_info_id: { primaryKey: true, type: Sequelize.INTEGER, autoIncrement: true},
    	user_profile_id: Sequelize.STRING,
		address_1: Sequelize.STRING,
		address_2: Sequelize.STRING,
		address_3: Sequelize.STRING,
		landmark: Sequelize.STRING,
		city: Sequelize.STRING,
		state: Sequelize.STRING,
		pincode: Sequelize.STRING,
	    latitude: Sequelize.DECIMAL(10,8),
    	longitude: Sequelize.DECIMAL(10,8),
		deleted: {type: Sequelize.STRING, defaultValue: "F"}
    });

	return instance;
};

module.exports = getInstance;