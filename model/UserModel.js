var Sequelize = require('sequelize');
var sequelize = require('../server/Sequelize')();

var instance;
var getInstance = function () {

    if(instance) return instance;

    instance = sequelize.define("user_profile", {
    	user_profile_id: { primaryKey: true, type: Sequelize.INTEGER, autoIncrement: true},
        first_name: Sequelize.STRING,
        second_name: Sequelize.STRING,
		last_name: Sequelize.STRING,
    	type:  Sequelize.STRING,
		email: Sequelize.STRING,
		gender: Sequelize.STRING,
		dateOfBirth: Sequelize.STRING,
		deleted: { type: Sequelize.STRING, defaultValue: "F"}

    });

    return instance;
};
module.exports = getInstance;