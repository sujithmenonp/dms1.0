"Delivery Management System" 
This is a NodeJS powered back-end system that allows customer management
It uses MYSQL datastore and 'Passport' module of NodeJS for security

Registration Request Sample: 
POST register

{
    "first_name": "Spider",
    "second_name": "Man",
    "last_name": "P",
    "email": "spiderman@gmail.com",
    "gender": "M",
    "dateOfBirth": "30-11-1992",
    "mobile_number": "7979797979",
    "address_1": "address_1",
    "address_2": "address_2",
    "address_3": "address_3",
    "state": "Karnataka",
    "city": "Bangalore",
    "pincode": "560034",
    "latitude": "12.990188",
    "longitude": "77.583020",
    "landmark": "Opposite To KFC",
    "type": "user/delivery"
    
}

Mobile Validation via OTP
PUT register
{
  "mobile_details_id": "1",
  "otp": "232",
  "user_profile_id": "1"
}

First Time Password Setting
POST login
{
  "password": "123462378",
  "mobile_details_id": "13"
}