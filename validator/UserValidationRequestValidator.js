var APIConstants = require("../constants/APIConstants");
var StringUtil = require("../util/StringUtil");

var checkMandatoryFields = function(req){
	
	if(StringUtil.isEmpty(req.body.otp)){
		return APIConstants.OTP_MISSING;
	}else if(StringUtil.isEmpty(req.body.mobile_details_id)){
		return APIConstants.MOBILE_DETAILS_ID_MISSING;
	}
	else if(StringUtil.isEmpty(req.body.user_profile_id)){
		return APIConstants.USER_PROFILE_ID_MISSING;
	}else{
		return "";
	}
}
module.exports = {
		checkMandatoryFields: checkMandatoryFields
}