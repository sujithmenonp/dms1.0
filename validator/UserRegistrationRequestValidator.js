var APIConstants = require("../constants/APIConstants");
var StringUtil = require("../util/StringUtil");

var validateParams = function(req, res) {
    	
        req.checkBody('first_name', APIConstants.FIRST_NAME_MISSING).notEmpty();
        req.checkBody('second_name', APIConstants.SECOND_NAME_MISSING).notEmpty();
        req.checkBody('address_1', APIConstants.ADDRESS_1_MISSING).notEmpty();
        req.checkBody('address_2', APIConstants.ADDRESS_2_MISSING).notEmpty();
        req.checkBody('city', APIConstants.CITY_MISSING).notEmpty();
        req.checkBody('address_2', APIConstants.PINCODE_MISSING).notEmpty();
        req.checkBody('state', APIConstants.STATE_MISSING).notEmpty();
        req.checkBody('mobile_number', APIConstants.MOBILE_NUMBER_MISSING).notEmpty();
        var errors = req.validationErrors();
        return errors;
}

module.exports = {
 	validateParams: validateParams
};
    