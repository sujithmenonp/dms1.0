var APIConstants = require("../constants/APIConstants");
var StringUtil = require("../util/StringUtil");


var validatePasswordField = function(req){
	
	var password = req.body.password;
	if(password.length <8){
		return APIConstants.ERROR_VALIDATING_PASSWORD;
	}
	else {
		return "";
	}
}

var validateParamsForPOSTPassword = function(req){
	
	if(StringUtil.isEmpty(req.body.password)){
		return APIConstants.PASSWORD_MISSING;
	}
	else if(StringUtil.isEmpty(req.body.mobile_details_id)){
		return APIConstants.MOBILE_DETAILS_ID_MISSING;
	}else{
		var success = validatePasswordField(req);
		return success;
	}
}

var validateParamsForAuthenticate = function(req){
	if(StringUtil.isEmpty(req.body.password)){
		return APIConstants.PASSWORD_MISSING;
	}
	else if(StringUtil.isEmpty(req.body.mobile_number)){
		return APIConstants.MOBILE_NUMBER_MISSING;
	}
	else{
		return "";
	}
}
module.exports = {
		validateParamsForPOSTPassword: validateParamsForPOSTPassword,
		validateParamsForAuthenticate: validateParamsForAuthenticate
}