var Sequelize = require('sequelize');
var sequelize = require('./Sequelize')();
var app = require('./Express')();
var Express = require('express');
var BodyParser = require("body-parser");
var CookieParser = require('cookie-parser');
var ExpressSession = require('express-session');
var SequelizeStore = require('connect-session-sequelize')(ExpressSession.Store);
var Passport = require('passport');
var Config = require('./Config');
var ExpressValidator = require('express-validator');
var Logger = require('../logger/Logger')();

var getServerInstance = function(){

  var server = app;
  if(Config.SERVER.HTTPS_ENABLED){

	  Logger.info("HTTPS is Enabled: Reading Certificates and Private Key");
	  var fs = require('fs');
	  var https = require('https');

	  try{
		  server = https.createServer({
			  cert: fs.readFileSync(__dirname + '/keys/dms.crt'),
			  key: fs.readFileSync(__dirname+'/keys/dms.key')
		  }, app)
	  }
	  catch (e){
		  Logger.error(e);
	  }
	  Logger.info("Successfully Loaded Certificates and Private Key");
  }
  return server;
}

var configure = function(){

	Logger.info('Start: Configuring Express Instance');
	app.use(BodyParser.json());
	app.use(Express.static('public'));
	app.use(CookieParser());
    app.use(ExpressValidator());
	app.use(ExpressSession({
		secret: process.env.SESSION_SECRET || Config.AUTHENTICATION.SESSION_SECRET,
		resave: Config.AUTHENTICATION.SESSION_RESAVE, 
		saveUninitialized: Config.AUTHENTICATION.SESSION_SAVE_UNINITIALIZED,
		store: new SequelizeStore({
		db: sequelize
		})
	}));
	app.use(Passport.initialize());
	app.use(Passport.session());

	Logger.info('Completed: Configuring Express Instance');

}

module.exports = {
	getServerInstance: getServerInstance,
	configure: configure
}