var Sequelize = require('sequelize');
var Config = require('./Config');
var Logger = require('../logger/Logger')();

    var instance;

    var getInstance = function (){

        if(instance) return instance;

        instance = new Sequelize(
            Config.DATABASE.DATABASE_NAME,
            Config.DATABASE.DATABASE_USER,
            Config.DATABASE.DATABASE_PASSWORD,
            {
                host: Config.DATABASE.DATABASE_HOST,
                port: Config.DATABASE.DATABASE_PORT ,
                dialect: Config.DATABASE.DATABASE_TYPE,
                logging: false
            }
        ) ;

        instance.sync().then(function (success) {
            if(!success){
                Logger.error('Sequelize: Data Model Construction Failed: ');
                instance = null;
            }else{
              //  Logger.info('Generated DB Models');
            }
        });

        Logger.info("Created Singleton Sequelize Instance");
        return instance;
    }

module.exports = getInstance