var ConfigConstants = require('../constants/ConfigConstants')

var SERVER = {

	 PORT: process.env.PORT || 3000,
     HOST: '127.0.0.1',
     HTTPS_ENABLED: true
}

var DATABASE = {

	 DATABASE_NAME: "dms",
     DATABASE_USER: "root",
     DATABASE_PASSWORD: "Welcome100",
     DATABASE_HOST: "127.0.0.1",
     DATABASE_PORT: 3306,
     DATABASE_TYPE: "mysql"
}

var AUTHENTICATION = {
	AUTHENTICATION_TYPE: ConfigConstants.LOCAL_AUTHENTICATION_TYPE,
	SESSION_SECRET: "23424240",
	SESSION_RESAVE: false,
	SESSION_SAVE_UNINITIALIZED: false
}

exports.SERVER = SERVER;
exports.DATABASE = DATABASE;
exports.AUTHENTICATION = AUTHENTICATION;