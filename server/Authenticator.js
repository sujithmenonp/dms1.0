var Passport = require('passport');
var PassportLocal = require('passport-local');
var ConfigConstants = require('../constants/ConfigConstants');
var Config = require('../server/config');
var EncryptionUtil = require('../util/EncryptionUtil');
var Logger = require('../logger/Logger')();
var UserLoginService = require('../service/UserLoginService');

var configure = function(){

    if(Config.AUTHENTICATION.AUTHENTICATION_TYPE === ConfigConstants.LOCAL_AUTHENTICATION_TYPE){

        Logger.info('Start: Configuring Passport To Use Local Strategy.');
         Passport.use(new PassportLocal.Strategy(UserLoginService.verifyUserPassword));

         Passport.serializeUser(function(user, done){
             done(null,  user.id);
         });

         Passport.deserializeUser(function(id, done){
            done(null, {id: id});
         });
        Logger.info('Completed: Configuring Passport To Use Local Strategy.');
    }
}


var isUserAuthenticated = function(req, res, next){

    if(req.user){
        Logger.info("Authenticator: isUserAuthenticated:  Session Available For User");
      return next();
    }
    else{
        Logger.info("Authenticator: isUserAuthenticated: Session  UnAvailable For User");
      return res.status(401).json({
        error: 'User not authenticated'
      })
   }
};


module.exports = {
   isUserAuthenticated: isUserAuthenticated,
   configure: configure
}