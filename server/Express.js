var Express = require('express');
var Logger = require('../logger/Logger')();

var instance;

var getInstance = function () {

    if(instance){

        return instance;
    }

    Logger.info("Created Singleton Express Instance.");
    instance = Express();

    return instance;
};

module.exports=getInstance;