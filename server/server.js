var Config = require('./config');
var Core = require('./core');
var Route = require('../routes/route');
var Authenticator = require('./Authenticator');
var Logger = require('../logger/Logger')();

var serverCallBack = function(error) {
	var protocol = Config.SERVER.HTTPS_ENABLED?"https":"http";
	if(error){
         Logger.info("Error Starting Server" + error);
	}else{
		Logger.info("DMS Listening at "+ protocol+"://"+Config.SERVER.HOST+":"+Config.SERVER.PORT);
	}
}
var initialize = function()	{
	var server = Core.getServerInstance();

	Core.configure();

	Authenticator.configure();

	Route.configure();

	return server;

}

var server = initialize();

server.listen(Config.SERVER.PORT, serverCallBack);



