var Passport = require('passport');
var UserRegistrationService= require('../service/UserRegistrationService');
var UserValidationService= require('../service/UserValidationService');
var UserLoginService = require('../service/UserLoginService');
var Authenticator = require('../server/Authenticator');
var Config = require('../server/Config');
var app = require('../server/Express')();
var Logger = require('../logger/Logger')();

var configure = function(){

    Logger.info('Start: Route: Configuring Routes');

	app.get('/', function(req,res) {
		console.log('%d Request Received', process.pid);
		res.send(__dirname+"/"+index.html);
	});

	app.post('/login',  Passport.authenticate(Config.AUTHENTICATION.AUTHENTICATION_TYPE), function(req, res){
		if(req.isAuthenticated){
			res.json("Authenticated");
		}
	});

	app.get('/user', Authenticator.isUserAuthenticated, function(req, res){
	 	res.status(200).json({
       	 status: 'All Users'
    	});
	});

	app.get('/logout', function(req,res){
			req.logout();
			res.json("Logged Out");
	});

	app.post('/register', UserRegistrationService.register);

	app.put('/register', UserValidationService.validate);

	app.post('/register/update', UserLoginService.setPassword);

	Logger.info('Completed: Route: Configuring Routes');
}

module.exports  = {
	configure: configure
}