var expect = require("expect.js");
var Sequelize = require("sequelize");
var sequelize = new Sequelize("mysql://root:Welcome100@localhost:3306/dms", {logging: false});
var model = require("./../model/UserModel.js")(sequelize);
var userService = require("./../service/UserService.js")(sequelize);
 
describe("userService", function () {
    var mockResponse = function (callback) { return { send: callback }; };
    var newUser = { first_name: "ABC", second_name:"XYZ" };
 
    beforeEach(function (done) {
        sequelize.sync({ force: true}).then(function () { done(); });
    });
 
    it("Finding Created User", function (done) {
        //arrange
        model.User.create(newUser).then(function () {
            //act
            userService.get({}, mockResponse(function (data) {
                //assert
	                expect(data[0].first_name).to.eql(newUser.first_name);
                done();
            }))
        })
    });
    it("Creating User Test", function (done) {
        //arrange
        
    	var req = { body: newUser };
        //act
        userService.create(req, mockResponse(function (statusCode) {
            //assert
            expect(statusCode).to.eql(200);
            done();
        }))
    });
});